This script is for cleaning linux based media directories when organized by either video or audio. <br>Please pay attention to list of files to be deleted when it is ran. 

<important>Installation</important>
<br>
To install this script the first step is to move to a temporary install directory.<br>
I suggest<br>
<code>cd ~/Downloads</code>
Then we must pull the script from gitlab.com.<br>
<code>git clone https://gitlab.com/distrax/rid.git</code><br>
This next step is optional. Only if you want to make this run as an actual command. (Without adding the "./" prefix and from anywhere.)<br>
<code>sudo mv rid/rid /usr/bin && rm -r rid</code>
<br>This will inevitably make the script run as easily as any command.<br>
Next is to make it executable<br>
<code>sudo chmod +x /usr/bin/rid</code>
<br>The rid script is ready to use.<br>
If it was place in /usr/bin then just "cd" to the target directory and run "rid"<br>otherwise while in the target folder run <br><code>./home/$USER/Downloads/rid</code>
